This directory should contain a Smarty Installation.

Extract the 'libs' directory from a Smarty tarball located at
http://smarty.php.net/download.php to this location.

Please refer to ../README.txt as well.