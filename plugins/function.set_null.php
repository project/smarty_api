<?php

/**
 * set_null Plugin
 *
 * Smarty function plugin to allow setting template variable to null
 *
 * Examples:
 * {set_null var="var_name"}
 *
 * @version  0.1
 * @author   Travis Cline <travis dot cline at gmail dot com>
 * @param    array
 * @param    Smarty
 * @return   string
 */

function smarty_function_set_null($params, &$smarty)
{
    if (!isset($params['var'])) {
        $smarty->trigger_error("set_null: missing 'var' parameter");
        return;
    }

    $smarty->assign($params['var'], null);
}

?>
