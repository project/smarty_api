<?php

function smarty_resource_global_var_source($resource_name, &$source_content, &$smarty) {
  $path = explode('->', $resource_name);
  $var = $GLOBALS;
  foreach ($path as $key) {
    $var = (array)$var;
    $var = $var[$key];
  }
  $source_content = $var;
  
  return TRUE;
}

function smarty_resource_global_var_timestamp($resource_name, &$resource_timestamp, &$smarty) {
  $resource_timestamp = time();
  
  return TRUE;
}

function smarty_resource_global_var_secure($resource_name, &$smarty) {
  return TRUE;
}

function smarty_resource_global_var_trusted($resource_name, &$smarty) {
  return TRUE;
}

?>